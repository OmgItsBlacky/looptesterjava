import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.LinkedList;
import java.util.stream.Stream;

public class Main {

    public static int[] LongForLoopBubbleA(int[] data, int loopRange) {
        int i = 0;
        int j = 0;
        int temporary = 0;

        for (; i < loopRange; i++) {
            for (j = 0; j < loopRange - i; j++) {
                if (data[j] > data[j + 1]) {
                    temporary = data[j];
                    data[j] = data[j + 1];
                    data[j + 1] = temporary;
                }
            }
        }

        return data;
    }

    public static int[] LongForLoopBubbleB(int[] data, int loopRange) {
        int i = 0;
        int j = 0;
        int temporary = 0;

        for (; i < loopRange; i++) {
            for (j = 0, temporary = 0; j < loopRange - i; j++) {
                if (data[j] > data[j + 1]) {
                    temporary = data[j];
                }

                if (data[j] > data[j + 1]) {
                    data[j] = data[j + 1];
                }

                if (temporary > data[j + 1]) {
                    data[j + 1] = temporary;
                }
            }
        }

        return data;
    }

    public static int[] LongForLoopBubbleC(int[] data, int loopRange) {
        int i = 0;
        int j = 0;
        int temporary = 0;

        for (; i < loopRange; i++) {
            for (j = 0, temporary = 0; j < loopRange - i; j++) {
                temporary = data[j] > data[j + 1] ? data[j] : temporary;
                data[j] = data[j] > data[j + 1] ? data[j + 1] : data[j];
                data[j + 1] = temporary > data[j + 1] ? temporary : data[j + 1];
            }
        }

        return data;
    }

    public static int[] LongForLoopBubbleD(int[] data, int loopRange) {
        int i = 0;
        int j = 0;

        int temporary_A = 0;
        int temporary_B = 0;

        for (; i < loopRange; i++) {
            for (j = 0; j < loopRange - i; j++) {
                temporary_A = data[j];
                temporary_B = data[j + 1];

                data[j] = temporary_B < temporary_A ? temporary_B : temporary_A;
                data[j + 1] = temporary_B < temporary_A ? temporary_A : temporary_B;
            }
        }

        return data;
    }

    public static void main(String[] args) throws InvocationTargetException, IllegalAccessException, IOException {

        LinkedList<Long> loopExecutionDurations = new LinkedList<Long>();
        int numberOfLoopExecutions = 1;
        int dataSize = 100000;

        int[] dataA = new int[dataSize];

        int i = 0;
        BufferedReader br = new BufferedReader(new FileReader("/home/blacky/Documents/Random.txt"));
        String line = br.readLine();
        dataA[i] = Integer.parseInt(line);
        while ((line = br.readLine()) != null) {
            dataA[i] = Integer.parseInt(line);
            i++;
        }

        for (int j = 0; j < numberOfLoopExecutions; j++) {
            long start = System.nanoTime();
            LongForLoopBubbleD(dataA, dataSize - 1);
            long stop = System.nanoTime();
            loopExecutionDurations.add(stop - start);
        }


        System.out.println(loopExecutionDurations.stream().mapToDouble(val -> val).average().orElse(0.0));
    }
}
