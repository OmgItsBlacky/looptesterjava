import java.lang.reflect.InvocationTargetException;
import java.util.LinkedList;
import java.util.concurrent.TimeUnit;
import java.util.function.BiFunction;

public class LoopTester {

    private int[] data;
    private int dataSize;
    private BiFunction<int[],Integer,Integer>  executedLoop;
    private int loopRange;
    private int numberOfLoopExecutions;
    private int averageTestProcessorFrequency;
    private LinkedList<Long> loopExecutionDurations;

    private void DataInitialization()
    {
        for (int i = 0; i < dataSize; ++i)
        {
            this.data[i] = i;
        }
    }


    LoopTester(BiFunction<int[],Integer,Integer> executedLoop, int numberOfLoopExecutions, int loopRange, int dataSize)
    {
        this.executedLoop = executedLoop;
        this.numberOfLoopExecutions = numberOfLoopExecutions;
        this.loopRange = loopRange;
        this.dataSize = dataSize;
        this.data = new int[this.dataSize];
        DataInitialization();
        loopExecutionDurations = new LinkedList<Long>();
    }

    public void RunTest() throws InvocationTargetException, IllegalAccessException {
        for (int i = 0; i < numberOfLoopExecutions; i++)
        {
            long start = System.nanoTime();
            executedLoop.apply(data,dataSize);
            long stop = System.nanoTime();
            loopExecutionDurations.add(stop-start);
        }
    }

    public double GetAverageTimeInNanoseconds()
    {
        return loopExecutionDurations.stream().mapToDouble(val -> val).average().orElse(0.0);
    }

    public double GetAverageTimeInMilliseconds()
    {
        var timeMilliseconds = new LinkedList<Long>();

        for (Long result : loopExecutionDurations)
        {
            timeMilliseconds.add(TimeUnit.MILLISECONDS.convert(result,TimeUnit.NANOSECONDS));
        }

        return timeMilliseconds.stream().mapToDouble(val -> val).average().orElse(0.0);
    }

    public void DescribeHarvestedData()
    {
        String output =
                "Loop executions: " + numberOfLoopExecutions +
                        "\tLoop range: " + loopRange +
                        "\tData size: " + dataSize +
                        "\tAverage time of single loop execution: " +
                        GetAverageTimeInNanoseconds() + "[ns]";

        System.out.println(output);
    }
}
